package com.shahzaib.mindr.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.shahzaib.mindr.R;
import com.shahzaib.mindr.databinding.PopAddEducationBinding;
import com.shahzaib.mindr.lib.education.EducationEvent;
import com.shahzaib.mindr.lib.education.EducationViewModel;

public class PopAddEducationActivity extends AppCompatActivity {

    private EducationViewModel educationViewModel = new EducationViewModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopAddEducationBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_add_education);

        binding.setEducation(educationViewModel);
        binding.setHandler(new EducationEvent(this));
    }
}