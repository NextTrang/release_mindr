package com.shahzaib.mindr.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import com.shahzaib.mindr.Getter;
import com.shahzaib.mindr.R;
import com.shahzaib.mindr.Sharing;
import com.shahzaib.mindr.label.LbCode;
import com.shahzaib.mindr.label.LbKey;

public class PopManagerActivity extends AppCompatActivity {

    private int popCode = -1;
    private Intent mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_manager);

        mData = getIntent();
        popCode = mData.getIntExtra(LbKey.POP_CODE, -1);
        startPopup(popCode);
    }

    public void startPopup(int popCode) {
        switch (popCode) {
            case LbCode.GET_CV_FILE:
                Intent iFileManager = new Intent(Intent.ACTION_GET_CONTENT);
                iFileManager.setType("*/*");
                startActivityForResult(iFileManager, popCode);
                break;
            case LbCode.GET_VIDEO_FILE:
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takeVideoIntent, popCode);
                }
                break;
            case LbCode.ADD_WORK_EXP:
                Intent iAddWorkExp = new Intent(this, PopAddWorkExpActivity.class);
                startActivityForResult(iAddWorkExp, popCode);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                case LbCode.GET_CV_FILE:
                    Uri uri = data.getData();
                    String path = Getter.getPathFromUri(this, uri);
                    Sharing.Instance().getApplicantViewModel().cvViewModel.setLocation(path);
                    break;
                case LbCode.GET_VIDEO_FILE:
                    Uri videoUri = data.getData();
                    String videoPath = Getter.getPathFromUri(this,videoUri);
                    Sharing.Instance().getApplicantViewModel().setVideoLocation(videoPath);
                    break;
                default:
                    setResult(RESULT_OK, data);
            }
        }

        finish();
    }
}