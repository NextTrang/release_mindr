package com.shahzaib.mindr.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.shahzaib.mindr.R;
import com.shahzaib.mindr.databinding.PopAddWorkExpBinding;
import com.shahzaib.mindr.lib.work_exp.WorkExpEvent;
import com.shahzaib.mindr.lib.work_exp.WorkExpViewModel;

public class PopAddWorkExpActivity extends AppCompatActivity {

    private WorkExpViewModel workExpViewModel = new WorkExpViewModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopAddWorkExpBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_add_work_exp);

        binding.setWorkExp(workExpViewModel);
        binding.setHandler(new WorkExpEvent(this));
    }
}