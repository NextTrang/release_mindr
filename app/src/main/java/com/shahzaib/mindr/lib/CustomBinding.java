package com.shahzaib.mindr.lib;

import androidx.databinding.BindingAdapter;

import com.google.android.material.textfield.TextInputLayout;

public class CustomBinding {
    @BindingAdapter({"android:inputError"})
    public static void loadError(TextInputLayout textInputLayout, String error) {
        textInputLayout.setError(error);
    }
}
