package com.shahzaib.mindr.lib.applicant;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.shahzaib.mindr.label.LbCode;
import com.shahzaib.mindr.label.LbKey;
import com.shahzaib.mindr.lib.api.RequestApplyApplicant;
import com.shahzaib.mindr.lib.cv.CV;
import com.shahzaib.mindr.pop_up.PopManagerActivity;

public class ApplicantEvent {
    Context context;

    public ApplicantEvent(Context context) {
        this.context = context;
    }

    public void onGetCVFromFileManager(View view) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbKey.POP_CODE, LbCode.GET_CV_FILE);
        ((AppCompatActivity) context).startActivityForResult(intent, LbCode.GET_CV_FILE);
    }

    public void onGetVideoFromCamera(View view) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbKey.POP_CODE, LbCode.GET_VIDEO_FILE);
        ((AppCompatActivity) context).startActivityForResult(intent, LbCode.GET_VIDEO_FILE);
    }

    public void onOpenAddWorkExp(View view) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbKey.POP_CODE, LbCode.ADD_WORK_EXP);
        ((AppCompatActivity) context).startActivityForResult(intent, LbCode.ADD_WORK_EXP);
    }

    public void onOpenAddEducation(View view) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbKey.POP_CODE, LbCode.ADD_EDUCATION);
        ((AppCompatActivity) context).startActivityForResult(intent, LbCode.ADD_EDUCATION);
    }

    public void onApplyInfo(ApplicantViewModel viewModel) {

        JsonObject jsInput = new JsonObject();

        jsInput.addProperty("first_name", viewModel.getFirstName());
        jsInput.addProperty("last_name", viewModel.getLastName());
        jsInput.addProperty("email", viewModel.getEmail());
        jsInput.addProperty("phone", viewModel.getPhone());
        jsInput.addProperty("dob", viewModel.getDob());
        jsInput.addProperty("postal_address", viewModel.getPostalAddress());
        jsInput.addProperty("video_location", viewModel.videoLocation);


        viewModel.updateCampaignId();
        jsInput.addProperty("campaign_id", "4567");

        viewModel.cvViewModel.updateSkills();
        CV cv = new CV(viewModel.cvViewModel);
        Gson gson = new GsonBuilder().create();
        JsonElement element = gson.toJsonTree(cv, CV.class);
        jsInput.add("curriculum_vitae", element);

        new RequestApplyApplicant().execute(context, jsInput.toString());
    }
}
