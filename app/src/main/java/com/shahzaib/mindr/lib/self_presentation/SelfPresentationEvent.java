package com.shahzaib.mindr.lib.self_presentation;

import android.content.Context;
import android.content.Intent;

public class SelfPresentationEvent {
    private Context context;

    public SelfPresentationEvent(Context context) {
        this.context = context;
    }

    public void onClickCheckBox(SelfPresentationInfoViewModel viewModel, int checkIndex){
        boolean currentState = viewModel.checkBoxesState.getValue().get(checkIndex);
        viewModel.checkBoxesState.getValue().set(checkIndex,!currentState);

        if(viewModel.isCheckedAll()){
//            Intent intent = new Intent(context, MainActivity.class);
//            intent.putExtra(LbPop.LbKey.FRAGMENT_ID, LbPop.LbCode.VIDEO_RECORDING_FRAGMENT_ID);
//            context.startActivity(intent);
        }
    }
}
