package com.shahzaib.mindr.lib.education;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.shahzaib.mindr.Sharing;

public class EducationEvent {
    Context context;

    public EducationEvent(Context context) {
        this.context = context;
    }

    public void onAddEducation(EducationViewModel viewModel){
        Sharing.Instance().getApplicantViewModel().cvViewModel.educations.add(viewModel);
        ((AppCompatActivity) context).finish();
    }
}
