package com.shahzaib.mindr.lib.tool;

public enum ToolName {
    None,
    NavigateToVideoRecordingFragment,
    NavigateToSelfPresentationInfoFragment,
    NavigateToBasicInformationFragment
}
