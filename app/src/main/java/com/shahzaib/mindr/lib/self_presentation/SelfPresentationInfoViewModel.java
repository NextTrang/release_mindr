package com.shahzaib.mindr.lib.self_presentation;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class SelfPresentationInfoViewModel extends ViewModel {

    public MutableLiveData<ArrayList<Boolean>> checkBoxesState = new MutableLiveData<ArrayList<Boolean>>() {
        {
            setValue(new ArrayList<Boolean>() {
                {
                    add(false);
                    add(false);
                    add(false);
                    add(false);
                    add(false);
                    add(false);
                }
            });
        }
    };

    public Boolean isCheckedAll() {
        for (Boolean check : checkBoxesState.getValue()) {
            if (!check) {
                return false;
            }
        }

        return true;
    }
}
