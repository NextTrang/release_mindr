package com.shahzaib.mindr.lib.tool;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.databinding.Bindable;

import com.shahzaib.mindr.BR;

public class Tool extends MyTool {

    private Context context;
    @Bindable
    private Boolean visible=true;

    public Tool(ToolName name, Context context) {
        super(name);
        this.context = context;
    }

    //Getter
    public Boolean getVisible() {
        return visible;
    }

    //Setter
    public void setVisible(Boolean visible) {
        this.visible = visible;
        notifyPropertyChanged(BR.visible);
    }

    @Override
    public void execute(Object objects) {
        switch (super.getName()) {
            case None:
                break;
            case NavigateToVideoRecordingFragment:
//                Intent iVideoRecording = new Intent(context, MainActivity.class);
//                iVideoRecording.putExtra(LbPop.LbKey.FRAGMENT_ID, LbPop.LbCode.VIDEO_RECORDING_FRAGMENT_ID);
//                context.startActivity(iVideoRecording);
                break;
            case NavigateToBasicInformationFragment:
//                Intent iBasicInfo = new Intent(context, MainActivity.class);
//                iBasicInfo.putExtra(LbPop.LbKey.FRAGMENT_ID, LbPop.LbCode.BASIC_INFORMATION_FRAGMENT_ID);
//                context.startActivity(iBasicInfo);
                break;
            case NavigateToSelfPresentationInfoFragment:
//                Intent iSelfPresent = new Intent(context, MainActivity.class);
//                iSelfPresent.putExtra(LbPop.LbKey.FRAGMENT_ID, LbPop.LbCode.SELF_PRESENTATION_INFO_FRAGMENT_ID);
//                context.startActivity(iSelfPresent);
                break;
            default:
                Log.e("Tool", "execute: undefine tool name");
                break;
        }
    }
}
