package com.shahzaib.mindr.lib.work_exp;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.shahzaib.mindr.Sharing;

public class WorkExpEvent {
    Context context;

    public WorkExpEvent(Context context) {
        this.context = context;
    }

    public void onAddWorkExp(WorkExpViewModel workExpViewModel){
        Sharing.Instance().getApplicantViewModel().cvViewModel.workExps.add(workExpViewModel);
        ((AppCompatActivity) context).finish();
    }
}
