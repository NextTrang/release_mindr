package com.shahzaib.mindr.lib.tool;

import androidx.databinding.BaseObservable;

public abstract class MyTool extends BaseObservable {
    private ToolName name;

    public MyTool(ToolName name) {
        this.name = name;
    }

    public abstract void execute(Object object);

    //Getter
    public ToolName getName() {
        return name;
    }

    //Setter
    public void setName(ToolName name) {
        this.name = name;
    }
}
