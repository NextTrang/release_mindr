package com.shahzaib.mindr.label;

public class LbCode {
    public static final int GET_CV_FILE = 0;
    public static final int GET_VIDEO_FILE = 1;
    public static final int ADD_WORK_EXP = 2;
    public static final int ADD_EDUCATION = 3;
}
